/*
 * SPItransferTypes.h
 *
 *  Created on: Jul 6, 2021
 *      Author: Freire
 */

#ifndef SPITRANSFERTYPES_H_
#define SPITRANSFERTYPES_H_

#include "stdio.h"
#include "ECOSAT_Bus_Comms.h"
#include "luaMAVSystemDefines.h"

//#define	__packed	__attribute__((__packed__))

#define DELIMITER			0x55
#define FROMSOMCMD			0x5A
#define FROMUCCMD1			0x50
#define FROMUCCMD2			0x51
#define FROMUCCMD3			0x52

typedef struct __attribute__((packed))
{
	vane_info_t vane1;
	vane_info_t vane2;
	anemometer_info_t anemometer1;
	anemometer_info_t anemometer2;
	ecosat_scaled_pressure_t ads1;
	ecosat_scaled_pressure_t ads2;
	ecosat_scaled_pressure_t ads3;
	servo_feedback_t servoFB0;
	servo_feedback_t servoFB1;
	servo_feedback_t servoFB2;
	servo_feedback_t servoFB3;
	servo_feedback_t servoFB4;
	MOTOR_RX_t motorFB1;
	MOTOR_RX_t motorFB2;
	septentrio_info_t septentrioGPS;
	dual_info_t dualGNSS;
	unitStatus_t StatusFCB;
	unitStatus_t StatusEcuD1;
	unitStatus_t StatusEcuD2;
	unitStatus_t StatusSeptentrio;
	unitStatus_t StatusDualGNSS;
}stuCToSOM;

typedef struct __attribute__((packed))
{
	battery_info_t battery1;
	battery_info_t battery2;
	battery_info_t battery3;
	battery_info_t battery4;
	battery_info_t battery5;
	battery_info_t battery6;
	battery_info_t battery7;
	battery_info_t battery8;
	battery_info_t battery9;
	battery_info_t battery10;
	PDU_STATUS_t pduStatus;
}stuCToSOM2;


typedef struct __attribute__((packed))
{
	envelope_actuator_feedback_t envelopeActuatorFeedback;
	sensor_array_info_t sensorArray1;
	sensor_array_info_t sensorArray2;
	sensor_array_info_t solarPanelsTemps;
}stuCToSOM3;

typedef struct __attribute__((packed))
{
	uint8_t ui8Head1;
	uint8_t ui8Head2;
	uint8_t ucCmdId;
	uint8_t ucPayloadSize;
	servo_reference_t servoRef0;
	servo_reference_t servoRef1;
	servo_reference_t servoRef2;
	servo_reference_t servoRef3;
	servo_reference_t servoRef4;
	MOTOR_TX_t motorRef1;
	MOTOR_TX_t motorRef2;
	uint8_t checksum;
	uint8_t nCanMsg;
	mavlink_command_can_bus_t canArray[CAN_MSGQ_SIZE];
	ARMING_t armingState;
}stSOMTouC;

typedef union __attribute__((packed))
{
	stuCToSOM ToSOM;
	stuCToSOM2 ToSOM2;
	stuCToSOM3 ToSOM3;
	stSOMTouC FromSOM;
}unionSPI;

typedef struct __attribute__((packed))
{
	uint8_t ui8Head1;
	uint8_t ui8Head2;
	uint8_t ucCmdId;
	uint8_t HeartBeat;
	unionSPI data;
	uint32_t checksum;
}uSPIData;


#endif /* SPITRANSFERTYPES_H_ */
