#ifndef INC_ECOSAT_BUS_COMMS_H_
#define INC_ECOSAT_BUS_COMMS_H_

#include "stdint.h"
#include "stdio.h"
#include "ecosat/mavlink.h"

typedef struct  __attribute__((packed))
{
	uint8_t	unitID;
	uint8_t unitCanTxCount;
	uint8_t unitCanRxCount;
	uint8_t unitSpiTransferCount;
}unitStatus_t;

typedef struct {
	uint8_t heartbeat;
	uint8_t status[3];
	int16_t temperature[2];
	int16_t pressure;
} SensorArray_t;

typedef struct {
	uint8_t heartbeat;
	uint8_t status;
	int16_t pressure;
} PressureArray_t;

typedef struct {
	PressureArray_t arrayPressure1;
	PressureArray_t arrayPressure2;
	PressureArray_t arrayPressure3;
} PressureArrayPack_t;


typedef struct __attribute__((packed))
{
 uint8_t hearbeat;
 int32_t lat; /*< [degE7] Latitude (WGS84)*/
 int32_t lon; /*< [degE7] Longitude (WGS84)*/
 int16_t alt; /*< [m] Altitude (MSL). Positive for up.*/
 uint8_t hdop; /*< [m] GPS HDOP horizontal dilution of position*/
 uint8_t vdop; /*< [m] GPS VDOP vertical dilution of position*/
 int16_t vn; /*< [m/s] x100 GPS velocity in NORTH direction in earth-fixed NED frame*/
 int16_t ve; /*< [m/s] x100 GPS velocity in EAST direction in earth-fixed NED frame*/
 int16_t vd; /*< [m/s] x100 GPS velocity in DOWN direction in earth-fixed NED frame */
// float speed_accuracy; /*< [m/s] GPS speed accuracy*/
// float horiz_accuracy; /*< [m] GPS horizontal accuracy*/
// float vert_accuracy; /*< [m] GPS vertical accuracy*/
// uint16_t ignore_flags; /*<  Bitmap indicating which GPS input flags fields to ignore.  All other fields must be provided.*/
 uint16_t time_week; /*<  GPS week number*/
 uint32_t time_week_ms; /*[ms] x0.001*/
// uint8_t gps_id; /*<  ID of the GPS for multiple GPS inputs*/
 uint8_t fix_type; /*<  0-1: no fix, 2: 2D fix, 3: 3D fix. 4: 3D with DGPS. 5: 3D with RTK*/
 uint8_t satellites_visible; /*<  Number of satellites visible.*/
} ecosat_gps_input_t;


typedef struct __attribute__((packed)){
 uint8_t hearbeat; /*< [ms] Timestamp (time since system boot)*/
 float press_abs; /*< [hPa] Absolute pressure*/
 uint16_t press_diff; /*< [hPa] Differential pressure 1*/
 int16_t temperature; /*< [cdegC] Temperature.TODO: max and min values*/
} ecosat_scaled_pressure_t;

typedef struct __attribute__((packed))
{
	uint8_t timestamp;
	uint8_t sensorStatus;		/*FLASGS TBD*/
	float WIND_ANGLE;			/*Wind direction: +-180.00º*/
}vane_info_t;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	uint8_t sensorStatus;	/*FLASGS TBD*/
	uint16_t WIND_SPEED;		/*wind speed: xxx.x format, m/s*0.1 units.*/
	uint16_t WIND_ANGLE;	/*Wind direction: 0 to 360 º. */
	uint8_t WIND_STATUS;	/*sensor status: 0 value means correct instead 1 or 2 are error status*/
	int16_t TEMPERATURE;	/*Temperature degCx0.1*/
	char TEMP_STATUS;	/*Temperature status V = valid, A = valid but in process could be error*/
}anemometer_info_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	uint16_t sensorStatus;		/*FLASGS TBD*/
	int32_t staticPressure;	/* TODO */
	int32_t totalPressure; /* TODO */
	int32_t temperature; /* TODO */
}ads_info_t;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;			/*heartbeat*/
	uint8_t sensorStatus;		/*NOMINAL=3,DEGRADED=2,EMERGENCY=1,UNKNOWN=0*/
	uint8_t actuator_ID;		/*Actuator ID 0x01-0x1E*/
	int32_t ticks;				/*ticks range [1000,2000]. center = 1500*/
	uint16_t status_word;		/*status word.*/
}servo_feedback_t;

typedef struct  __attribute__((packed))
{
	uint8_t servoDataOk; /* TODO */
	uint16_t ticks; /* TODO */
}servo_reference_t;

typedef struct  __attribute__((packed))
{
	uint8_t armingStatus;
	uint8_t armingDataOk;
	uint16_t referenceServo0;
}ecu_d1_backup_t;

typedef struct  __attribute__((packed))
{
	uint8_t armingStatus;
	uint8_t armingDataOk;
	uint8_t referenceServo1;
	uint8_t referenceServo2;
	uint8_t referenceServo3;
	uint8_t referenceServo4;
	uint8_t servosDataOk;
}ecu_d2_backup_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	unitStatus_t unitStatus;			/*FLASGS TBD*/
	uint16_t habilitation_control; /* TODO: documentation */
	uint8_t power_status; /* TODO: documentation */
	uint8_t mode_status; /* TODO: documentation */
	float rpm; /* TODO */
	uint8_t direction; /* TODO: documentation */
	float driver_temperature; /* TODO */
	float motor_temperature; /* TODO */
	float internal_temperature; /* TODO */
	float power; /* TODO */
	float current; /* TODO */
	float voltage; /* TODO */
}engine_feedback_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	unitStatus_t unitStatus;			/*FLASGS TBD*/
	uint16_t habilitation_control; /* TODO: documentation */
	uint8_t power_status; /* TODO: documentation */
	uint8_t mode_status; /* TODO: documentation */
	uint16_t pwm; /* TODO */
	uint8_t direction; /* TODO: documentation */
}engine_reference_t;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	uint8_t sensorStatus;        /*FLASGS TBD*/
	ecosat_gps_input_t gps_inputs;
}septentrio_info_t;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	uint8_t sensorStatus;        		/*FLASGS TBD*/
	ecosat_gps_input_t gps_inputs;
    uint16_t baseline;					/*scaled e-02. realative distance between both gnss. Units cm*/
    float heading;					/*scaled e-05. Yaw of vehicle relative to Earth's North, use 0 for north*/
}dual_info_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	uint8_t id;
	uint8_t battery_function;
	uint8_t type;
	int16_t temperature;
	uint16_t voltages[10];
	int16_t current_battery;
	int32_t current_consumed;
	int32_t energy_consumed;
	int8_t battery_remaining;
}battery_info_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	uint8_t unitHeartbeat;
	uint8_t unitIntegrity;			/*@IntegrityValues*/
}actuator_feedback_t;

typedef struct  __attribute__((packed))
{
	actuator_feedback_t valveFB1;
	actuator_feedback_t valveFB2;
	actuator_feedback_t valveFB3;
	actuator_feedback_t valveFB4;
	actuator_feedback_t valveFBAir;
	actuator_feedback_t fanHe_1f;
	actuator_feedback_t fanHe_1b;
	actuator_feedback_t fanHe_2f;
	actuator_feedback_t fanHe_2b;
	actuator_feedback_t fanHe_3f;
	actuator_feedback_t fanHe_3b;
	actuator_feedback_t blower_1;
	actuator_feedback_t blower_2;
}envelope_actuator_feedback_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	unitStatus_t unitStatus;			/*FLASGS TBD*/
	float setPoint;
}actuator_reference_t;

typedef struct  __attribute__((packed))
{
	uint32_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	uint8_t heartbeats[5];
	uint16_t tempStatus[5];			/* 2 bits status */
	uint16_t baroStatus[2];			/* 2 bits status */
	int16_t tempValues[45];			/* ºC celsius x100 */
	int16_t baroValues[5];			/* bar x10000	*/
	uint16_t tachometer;
}sensor_array_info_t;

typedef struct  __attribute__((packed))
{
	uint8_t heartbeat;
	uint8_t dataOk;
	uint32_t armingCode; /* Armed=14041998. Else, DISARMED */
}ARMING_t;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;				/*FLASGS TBD*/
	servo_reference_t servoOut0; 		/*servo angle scaled x100*/
	ARMING_t armingState;				 /* Armed=14041998. Else, DISARMED */
}MSG_CAN_1;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	servo_reference_t servoOut1;	/*servo ticks: [min,max]=[1000,2000], 1500 is center position of servo*/
	servo_reference_t servoOut2;	/*servo ticks: [min,max]=[1000,2000], 1500 is center position of servo*/
	servo_reference_t servoOut3;	/*servo ticks: [min,max]=[1000,2000], 1500 is center position of servo*/
	servo_reference_t servoOut4;	/*servo ticks: [min,max]=[1000,2000], 1500 is center position of servo*/
	ecosat_scaled_pressure_t ads;
	ARMING_t armingState;
}MSG_CAN_2;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	servo_feedback_t servo0;
	vane_info_t vane0;
	vane_info_t vane1;
	anemometer_info_t anemometer0;
	anemometer_info_t anemometer1;
	ecosat_scaled_pressure_t ads;
}MSG_CAN_3;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	servo_feedback_t servo0;
	vane_info_t vane0;
	vane_info_t vane1;
	ecosat_scaled_pressure_t ads;
}MSG_CAN_3_A;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	servo_feedback_t servo0;
	anemometer_info_t anemometer0;
	anemometer_info_t anemometer1;
	ecosat_scaled_pressure_t ads;
}MSG_CAN_3_B;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	servo_feedback_t servo1;
	servo_feedback_t servo2;
	servo_feedback_t servo3;
	servo_feedback_t servo4;
	ecosat_scaled_pressure_t ads;
}MSG_CAN_4;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	septentrio_info_t septentrio;
}MSG_CAN_5;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	dual_info_t dualGnss;
}MSG_CAN_6;
typedef struct  __attribute__((packed))
{
	uint8_t dataOk;
	uint8_t pwmMode;
	int16_t pressure;
}PRESSURE_IMPUTS_t;
typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	PRESSURE_IMPUTS_t pressInput;
	uint16_t tachometer;
}MSG_CAN_8;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	uint8_t heartbeats[5];
	uint16_t statusTemp[5];
	uint16_t arrayTemp[33];
	uint8_t pressStatus[2];
	uint16_t arrayPress[5];
	uint16_t tachometer;
}MSG_CAN_9;

typedef struct  __attribute__((packed))
{
	mavlink_message_t microhardMsg;
}UHARD_MSG;

typedef struct  __attribute__((packed))
{
	uint16_t rcChannelOverride[5];
}CHANNEL_OVERRIDE;

typedef struct  __attribute__((packed))
{
	uint8_t jetiStatus;
	uint8_t rlcGroundStatus;
	uint8_t rlcAirStatus;
	uint8_t serialGcsStatus;
	uint8_t	serialAutopilotStatus;
	uint8_t microhardGroundStatus;
	uint8_t microhardAirStatus;
}COMMS_INTEGRITY;



/*
 * typedef PDU Rx messages
 */

typedef struct  __attribute__((packed))
{
	uint64_t cmuStatusA;

}t_CMU_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint64_t cmuStatusB;

}t_CMU_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
}t_ICA1_STATUS;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
}t_ICA2_STATUS;

typedef struct  __attribute__((packed))
{
	uint32_t ica3Status;
}t_ICA3_STATUS;

typedef struct  __attribute__((packed))
{
	uint32_t ica4Status;
}t_ICA4_STATUS;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
	uint8_t currentPMD11;
	uint8_t currentPMD21;
	uint8_t currentPMD31;
	uint8_t currentPMD41;
	uint8_t currentPMD51;
}t_INTA_1_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint8_t currentPMD101;
	uint8_t currentPMD111L;
}t_INTA_1_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
	uint8_t currentPMD61;
	uint8_t currentPMD71;
	uint8_t currentPMD81;
	uint8_t currentPMD91;
	uint8_t currentPMD111R;
}t_INTA_2_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint8_t currentPMD121;
}t_INTA_2_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint64_t IntB1StatusA;
}t_INTB_1_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
}t_INTB_1_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint64_t IntB1StatusA;
}t_INTB_2_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
}t_INTB_2_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t currentPMDB11;
	uint8_t currentPMDB12;
	uint8_t currentPMDB13;
	uint8_t currentPMDB14;
	uint8_t currentPMDB102;
}t_INTB_3_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint16_t VoltagePMD102;
}t_INTB_3_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t currentPMDM11;
	uint8_t currentPMDM12;
	uint8_t currentPMDM13;
	uint8_t currentPMDM14;
	uint8_t currentPMD112;
}t_INTB_4_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint16_t VoltagePMD112;
}t_INTB_4_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t currentPMDMB21;
	uint8_t currentPMDMB22;
	uint8_t currentPMDMB23;
	uint8_t currentPMDMB24;
	uint8_t currentPMD122;
}t_INTB_5_STATUS_A;

typedef struct  __attribute__((packed))
{
	uint16_t VoltagePMD122;
}t_INTB_5_STATUS_B;

typedef struct  __attribute__((packed))
{
	uint8_t byte0;
	uint8_t byte1;
	uint8_t byte2;
	uint8_t byte3;
	uint8_t byte4;
}t_INTB_6_STATUS;

typedef struct  __attribute__((packed))
{
	uint16_t pduMode;
}t_PDU_MODE;


typedef struct  __attribute__((packed))
{
	uint8_t cmuStatusAHeartBeat;
	uint8_t cmuStatusBHeartBeat;
	uint8_t ica1StatusHeartBeat;
	uint8_t ica2StatusHeartBeat;
	uint8_t ica3StatusHeartBeat;
	uint8_t ica4StatusHeartBeat;
	uint8_t intA1StatusAHeartBeat;
	uint8_t intA1StatusBHeartBeat;
	uint8_t intA2StatusAHeartBeat;
	uint8_t intA2StatusBHeartBeat;
	uint8_t intB1StatusAHeartBeat;
	uint8_t intB1StatusBHeartBeat;
	uint8_t intB2StatusAHeartBeat;
	uint8_t intB2StatusBHeartBeat;
	uint8_t intB3StatusAHeartBeat;
	uint8_t intB3StatusBHeartBeat;
	uint8_t intB4StatusAHeartBeat;
	uint8_t intB4StatusBHeartBeat;
	uint8_t intB5StatusAHeartBeat;
	uint8_t intB5StatusBHeartBeat;
	uint8_t intb6StatusHeartBeat;
	uint8_t pduModeHeartBeat;

}t_PDU_HEARTBEATS;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;
	t_CMU_STATUS_A cmuStatusA;
	t_CMU_STATUS_B cmuStatusB;
	t_ICA1_STATUS ica1Status;
	t_ICA2_STATUS ica2Status;
	t_ICA3_STATUS ica3Status;
	t_ICA4_STATUS ica4Status;
	t_INTA_1_STATUS_A intA1StatusA;
	t_INTA_1_STATUS_B intA1StatusB;
	t_INTA_2_STATUS_A intA2StatusA;
	t_INTA_2_STATUS_B intA2StatusB;
	t_INTB_1_STATUS_A intB1StatusA;
	t_INTB_1_STATUS_B intB1StatusB;
	t_INTB_2_STATUS_A intB2StatusA;
	t_INTB_2_STATUS_B intB2StatusB;
	t_INTB_3_STATUS_A intB3StatusA;
	t_INTB_3_STATUS_B intB3StatusB;
	t_INTB_4_STATUS_A intB4StatusA;
	t_INTB_4_STATUS_B intB4StatusB;
	t_INTB_5_STATUS_A intB5StatusA;
	t_INTB_5_STATUS_B intB5StatusB;
	t_INTB_6_STATUS intb6Status;
	t_PDU_MODE pduMode;
	t_PDU_HEARTBEATS pduHeartBeats;
}PDU_STATUS_t;

/********************************* motor struct *****************************/

/*
 * TX MESSAGE
 */
typedef struct  __attribute__((packed))
{
	uint8_t	idDevice; /* TODO: documentation */
	uint8_t	idInternal; /* TODO: documentation */
	uint8_t	Heartbeat; /* TODO: documentation */
}HEARTBEAT_t;

typedef struct  __attribute__((packed))
{
	uint8_t	idDevice; /* TODO: documentation */
	uint8_t	idInternal; /* TODO: documentation */
	uint8_t	ResponseSelectData; /* TODO: documentation */
	uint8_t	PowerControl; /* TODO: documentation */
	uint8_t	DirectionControl; /* TODO: documentation */
	uint8_t	BreakControl; /* TODO: documentation */
	uint16_t PwmControl; /* TODO: documentation */
}CONTROL_t;

typedef struct  __attribute__((packed))
{
	uint8_t 		motorDataOk; /* TODO: documentation */
	HEARTBEAT_t		Heartbeat; /* TODO: documentation */
	CONTROL_t		control; /* TODO: documentation */
}MOTOR_TX_t;

/*
 * RX MESSAGE
 */

typedef struct  __attribute__((packed))
{
	uint8_t	idDevice; /* TODO: documentation */
	uint8_t	idInternal; /* TODO: documentation */
	uint8_t	Heartbeat; /* TODO: documentation */
}HEARTBEAT_RX_t;

typedef struct  __attribute__((packed))
{
	uint8_t	idDevice; /* TODO: documentation */
	uint8_t	idInternal; /* TODO: documentation */
	uint8_t	ConfigurationStatus; /* TODO: documentation */
	uint8_t	ModeStatus; /* TODO: documentation */
	uint16_t PowerStatus; /* TODO: documentation */
	uint16_t HabilitationStatus; /* TODO: documentation */
}STATUS_INFO_t;

typedef struct  __attribute__((packed))
{
	uint8_t	  idDevice;
	uint8_t	  idInternal;
	uint8_t	  direction; /* TODO: documentation */
	uint8_t	  breakControl; /* TODO: documentation */
	uint16_t  pwmControl; /* TODO: documentation */
	uint16_t  motorRpm; /* TODO: documentation */
}RETURN_RPM_t;

typedef struct  __attribute__((packed))
{
	uint8_t	  idDevice;
	uint8_t	  idInternal;
	uint16_t  DriverTemperature; /* TODO: documentation */
	uint16_t  MotorTemperature; /* TODO: documentation */
	uint16_t  InternalTemperature; /* TODO: documentation */
}RETURN_TEMPERATURES_t;

typedef struct  __attribute__((packed))
{
	uint8_t	  idDevice;
	uint8_t	  idInternal;
	uint16_t  VoltageDriver; /* TODO: documentation */
	uint16_t  CurrentDriver; /* TODO: documentation */
	uint16_t  PowerDriver; /* TODO: documentation */
}RETURN_ELECTRICAL_INFO_t;

typedef struct  __attribute__((packed))
{
	uint8_t timestamp;
	unitStatus_t unitStatus;		/*FLASGS TBD*/
	HEARTBEAT_RX_t				heartbeatRx;
	STATUS_INFO_t				statusInfo;
	RETURN_RPM_t				returnRpm;
	RETURN_TEMPERATURES_t		returnTemperature;
	RETURN_ELECTRICAL_INFO_t	returnElectricalInfo;
}MOTOR_RX_t;

typedef struct  __attribute__((packed))
{
	uint8_t IdDevice;
	uint8_t mode; /* TODO: documentation */
	uint8_t pwm1; /* TODO */
	uint8_t pwm2; /* TODO */
	uint8_t pwm3; /* TODO */
	uint8_t pwm4; /* TODO */
	uint8_t pwm5; /* TODO */
	uint8_t pwm6; /* TODO */
}GASBAG_FAN_AIR_t;

typedef struct  __attribute__((packed))
{
	uint8_t Flags;			/*(bit0 = write blower1,bit1 = write blower2)*/
	uint8_t mode;			/*(0 = shut down, 1 = manual, 2= automatic)*/
	uint8_t pwm1; /* TODO */
	uint8_t pwm2; /* TODO */
}BLOWER_t;

typedef struct  __attribute__((packed))
{
	uint8_t dataOk;
	uint8_t pwm1; /* 0 to 100 pwm */
	uint8_t pwm2; /* 0 to 100 pwm */
}PWM_REFERENCE_t;



typedef struct  __attribute__((packed))
{
	uint8_t Flags;			/*bit0 = write valve1,bit1 = write valve2, bit2 = write valve3, bit3 = write valve4*/
	uint8_t valve1;			/*0xFF = Open, 0x00 = Close*/
	uint8_t valve2;			/*0xFF = Open, 0x00 = Close*/
	uint8_t valve3;			/*0xFF = Open, 0x00 = Close*/
	uint8_t valve4;			/*0xFF = Open, 0x00 = Close*/
}VALVE_t;

typedef struct  __attribute__((packed))
{
	uint8_t	mode;			/*1 = OPEN, 0 = CLOSE*/
}VALVE_AIR_t;

typedef struct  __attribute__((packed))
{
	uint8_t	modeFan1Forward;		/*1 = running, 0 = stopped*/
	uint8_t	modeFan1Backward;		/*1 = running, 0 = stopped*/
	uint8_t	modeFan2Forward;		/*1 = running, 0 = stopped*/
	uint8_t	modeFan2Backward;		/*1 = running, 0 = stopped*/
	uint8_t	modeFan3Forward;		/*1 = running, 0 = stopped*/
	uint8_t	modeFan3Backward;		/*1 = running, 0 = stopped*/
}INTEGRITY_FAN_F_t;

typedef struct  __attribute__((packed))
{
	uint8_t	valve1Status; /* TODO */
	uint8_t	valve2Status; /* TODO */
	uint8_t	valve3Status; /* TODO */
	uint8_t	valve4Status; /* TODO */
	uint8_t	valveAirStatus; /* TODO */
}VALVE_INTEGRITY_t;

typedef struct  __attribute__((packed))
{
	uint8_t msg_type;						/*@siotplinkers*/
	uint16_t size;
	uint8_t data[510];
}ISO_TP_MSG_t;

/*
 * ft2 type
 */
#define FT2_MSG_LEN			8
#define FT2_MSG_BYTE_O		0xFE
#define FT2_MSG_BYTE_1		0xAB
#define CRCH				0xDE
#define CRCL				0x76
typedef struct  __attribute__((packed))
{
	uint8_t  payload[FT2_MSG_LEN];
}ft2Msg_t;

/*
 * @siotplinkers
 */
#define ECU_D1_MSG			1
#define ECU_D2_MSG			2
#define ECU_D1_B_MSG		3
#define S_GNSS_MSG			4
#define D_GNSS_MSG			5

/*
 * @brief function status values
 */
#define FUN_STS_UNKNOWN		0
#define FUN_STS_EMERGENCY	1
#define FUN_STS_DEGRADED	2
#define FUN_STS_NOMINAL		3

/*
 * @brief DEFINE system and component mavlink ID
 */

#define LRC_SYSTEM_GROUND_ID				3 		/*TODO*/
#define LRC_SYSTEM_AIR_ID					4 		/*TODO*/
#define LRC_COMPONENT_GROUND_ID  			5 		/*TODO*/
#define LRC_COMPONENT_AIR_ID  				6 		/*TODO*/

/*
 * @brief arming code
 */

#define ARMED_CODE 				14041998
#define ARMED_CODE_1B			0xAA
#define SERVO_DATAOK 			0x0A
#define SERVO_1_DATAOK_OFFSET	0X01
#define SERVO_2_DATAOK_OFFSET	0X02
#define SERVO_3_DATAOK_OFFSET	0X04
#define SERVO_4_DATAOK_OFFSET	0X08
/*
 * @brief DEFINE valve/fan system type
 */

#define FAN_CONTROL_1
//#define FAN_CONTROL_2
//#define FAN_CONTROL_3

//#define VALVE_CONTROL_1
//#define VALVE_CONTROL_2
//#define VALVE_CONTROL_3
//#define VALVE_CONTROL_4

//#define VALVE_CONTROL_AIR

#endif /*INC_ECOSAT_BUS_COMMS_H_*/

