/*
 * type.h
 *
 *  Created on: Sep 21, 2021
 *      Author: Raul Berman
 */

#ifndef TYPE_H_
#define TYPE_H_
/*ECU D1*/
#define TYPE_A
//#define	TYPE_B
/*after change type, change UART baudrate
 *
 * TYPE A:
 * 	UART 1 = 115200
 * 	UART 5 = 115200
 * TYPE B:
 * 	UART 1 = 38400
 * 	UART 5 = 38400
 *
 * */

/*ECU D2*/
//#define ECU_D2_TYPE_A
#define ECU_D2_TYPE_B

/*ECU S*/
//#define ECU_S_TYPE_A		1
#define ECU_S_TYPE_B		2

#endif /* TYPE_H_ */
